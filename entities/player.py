import pygame
import transitions
from game.collision_detection import CollisionDetector
from game.entity_manager import EntityTypes

pygame.font.init()
font = pygame.font.Font(None, 36)

class Player:
    states = ['idle', 'running', 'jumping', 'falling', 'hanging', 'pulling_up']
    transitions = [
        ['run', 'idle', 'running'],
        ['run', 'falling', 'running'],
        ['up', 'idle', 'jumping'],
        ['up', 'running', 'jumping'],
        ['fall', 'idle', 'falling'],
        ['fall', 'running', 'falling'],
        ['fall', 'hanging', 'falling'],
        ['pull_up', 'hanging', 'pulling_up'],
        ['grab', 'falling', 'hanging'],
        ['grab', 'jumping', 'hanging'],
        ['stop', 'running', 'idle'],
        ['stop', 'pulling_up', 'idle'],
        ['jump_end', 'jumping', 'falling'],
        ['land', 'falling', 'idle'],
    ]

    def __init__(self, x, y, frame_width, frame_height, bounding_box_width, bounding_box_height, image, animation_manager, entity_manager):
        self.type = EntityTypes.PLAYER
        self.image = image
        self.animation = "idle"
        self.animation_manager = animation_manager

        # State
        self.machine = transitions.Machine(model=self, states=Player.states, transitions=Player.transitions, initial='idle')

        # Coordinates and dimensions
        self.x = x
        self.y = y
        self.x_record = x
        self.y_record = y
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.offset_left = ((frame_width - bounding_box_width) // 2) + 8
        self.offset_top = frame_height - bounding_box_height + 6
        self.rect = pygame.Rect(x + self.offset_left, y + self.offset_top, bounding_box_width, bounding_box_height)
        self.sensor = pygame.Rect(self.rect.x - (0.5*self.rect.width), self.rect.y - (0.5*self.rect.height), self.rect.width*2, self.rect.height*2)

        # Dynamics
        self.speed = 1.6
        self.jump_start_time = None
        self.x_velocity = 0
        self.y_velocity = 0
        self.gravity = -9.5
        self.direction = 1
        self.ground_detection = True
        self.collision_detector = CollisionDetector(self, entity_manager)
        self.platform_collisions = None

    def update_rect(self):
        self.rect.x = self.x + self.offset_left
        self.rect.y = self.y + self.offset_top
        self.sensor.x = self.rect.x - (0.5*self.rect.width)
        self.sensor.y = self.rect.y - (0.5*self.rect.height)

    def handle_input(self, events):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.state == 'idle':
            self.direction = -1
            self.trigger('run')
        elif keys[pygame.K_RIGHT] and self.state == 'idle':
            self.direction = 1
            self.trigger('run')
        
        for event in events:
            if event.type == pygame.KEYDOWN:
                if self.state in ('idle', 'running') and event.key == pygame.K_UP:
                    self.y_velocity = 8
                    self.trigger('up')
                if self.state == 'hanging' and event.key == pygame.K_UP:
                    self.trigger('pull_up') 
            elif event.type == pygame.KEYUP:
                if self.state == 'running' and (event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT):
                    self.trigger('stop')

    def update(self):
        self.platform_collisions = self.collision_detector.get_collisions('platforms')

        if self.state == 'idle':
            self.idle_update()
            self.position_correction()
        elif self.state == 'running':
            self.running_update()
        elif self.state == 'jumping':
            self.jumping_update()
        elif self.state == 'falling':
            self.falling_update()
        elif self.state == 'hanging':
            self.hanging_update()
        elif self.state == 'pulling_up':
            self.pulling_up_update()

        self.x += self.x_velocity
        self.update_rect()
        self.animation_manager.set_animation(self.animation)

    def fall_control(self):
        collision = self.collision_detector.get_collisions('platforms')
        if not collision.bottom and not collision.ground_close_by:
            self.trigger('fall')

    def grab_control(self):
        if (self.platform_collisions.grab.value == self.direction and pygame.key.get_pressed()[pygame.K_LSHIFT]):
            self.x = self.platform_collisions.grab.attach_point[0] - (self.rect.width * int(self.direction == 1) + self.offset_left + 2 * self.direction)
            self.y = self.platform_collisions.grab.attach_point[1]
            self.trigger('grab')
            return True
        else:
            return False

    def side_collision_control(self):
        if (self.platform_collisions.left and self.direction == -1 and not self.platform_collisions.top):
            print('on left: %s' % self.platform_collisions.left)
            self.x_velocity = 0
            # self.x_velocity = self.speed * -self.direction
            # self.x = self.x + self.platform_collisions.left_overlap
            return True
        elif(self.platform_collisions.right and self.direction == 1 and not self.platform_collisions.top):
            print('on right: %s' % self.platform_collisions.right)
            self.x_velocity = 0
            # self.x_velocity = self.speed * -self.direction
            # self.x = self.x - self.platform_collisions.right_overlap
            return True
        else:
            return False

    def idle_update(self):
        self.x_velocity = 0
        self.y_velocity = 0

        self.animation = 'idle'
        self.fall_control()

    def pulling_up_update(self):
        self.animation = 'pulling_up'
        if self.animation_manager.current_animation == 'pulling_up':
            if self.animation_manager.is_animation_finished():
                self.trigger('stop')
            else:
                progress = self.animation_manager.get_animation_progress()
                if (progress < 0.6):
                    self.y = self.y_record - (self.rect.height + 4) * progress
                elif (progress > 0.7):
                    self.y = self.y_record - (self.rect.height + 4) * progress
                    self.x = self.x_record + self.direction * (self.rect.width + 4) * (progress - 0.7) * 3.33
        else:
            self.x_record = self.x
            self.y_record = self.y

    def hanging_update(self):
        self.x_velocity = 0
        self.y_velocity = 0
        if pygame.key.get_pressed()[pygame.K_LSHIFT]:
            self.animation = 'hanging'
        else:
            self.trigger('fall')

    
    def position_correction(self):
        if self.platform_collisions.bottom_overlap > 0:
            self.y -= self.platform_collisions.bottom_overlap - 1
        if self.animation not in ('jump_start', 'jumping', 'falling') and self.platform_collisions.ground_close_by and self.platform_collisions.ground_distance > 0:
            self.y += self.platform_collisions.ground_distance + 1
        if self.state == 'jumping' and self.platform_collisions.top_overlap > 0:
            self.y += self.platform_collisions.top_overlap

    def running_update(self):
        self.x_velocity = self.speed * self.direction
        self.animation = 'run'

        self.fall_control()

        self.position_correction()
        self.side_collision_control()
        
    def jumping_update(self):
        if self.animation not in ('jump_start', 'jumping'):
            self.animation = 'jump_start'
            if self.x_velocity:
                self.x_velocity = self.x_velocity * 2
        elif self.animation_manager.is_animation_finished():
            self.animation = 'jumping'

        if self.x_velocity:
            if ((self.direction == -1 and not pygame.key.get_pressed()[pygame.K_LEFT])
            or (self.direction == 1 and not pygame.key.get_pressed()[pygame.K_RIGHT])):
                    self.x_velocity -= (self.x_velocity * 0.2)

        if self.x_velocity < 1:
            if pygame.key.get_pressed()[pygame.K_LEFT]:
                self.direction = -1
            if pygame.key.get_pressed()[pygame.K_RIGHT]:
                self.direction = 1

        if abs(self.x_velocity) < self.speed * 2:
            if ((self.direction == -1 and pygame.key.get_pressed()[pygame.K_LEFT])
            or (self.direction == 1 and pygame.key.get_pressed()[pygame.K_RIGHT])):
                self.x_velocity += self.speed * 0.2 * self.direction
            
        if self.animation == 'jumping':
            self.y -= self.y_velocity
            self.y_velocity -= 0.5

            if not self.grab_control():
                if self.y_velocity < 0:
                    self.trigger('jump_end')
                elif self.platform_collisions.top_overlap > 0:
                    self.y_velocity -= self.y_velocity * 0.8
                    self.trigger('jump_end')
                else:
                    self.position_correction()
                    if self.side_collision_control():
                        self.trigger('jump_end')
                
            
    def falling_update(self):
        self.animation = 'fall'
        self.y += self.y_velocity
        self.y_velocity += 0.5
        if not self.grab_control():
            if self.platform_collisions.bottom:
                self.y_velocity = 0
                self.trigger('land')

            self.position_correction()
            self.side_collision_control()


    def render(self, screen):
        current_frame = self.animation_manager.get_animation()
        if (self.direction < 0):
            current_frame = pygame.transform.flip(current_frame, True, False)
        screen.blit(current_frame, (self.x, self.y))

        # text = font.render(self.state, True, (255, 0, 0))
        # text2 = font.render(self.animation, True, (0, 0, 255))
        # screen.blit(text, (self.x, self.y - 50))
        # screen.blit(text2, (self.x, self.y - 70))

