import pygame
from game.entity_manager import EntityTypes

class Platform:
    def __init__(self, x, y, frame_width, frame_height, name):
        self.type = EntityTypes.PLATFORM
        self.name = name

        self.x = x
        self.y = y
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.rect = pygame.Rect(x, y, frame_width, frame_height)
        self.surface = pygame.Surface((frame_width, frame_height), pygame.SRCALPHA)

        self.surface.set_alpha(128)
        self.surface.fill((255, 0, 0))

    def get_rect(self):
        return self.rect

    def render(self, screen):
        # screen.blit(self.surface, (self.x, self.y))
        return None

