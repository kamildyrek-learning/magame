from game.entity_manager import EntityTypes
import pygame

class Background:
    def __init__(self, x, y, width, height, image, name):
        self.type = EntityTypes.BACKGROUND
        self.name = name
        self.image = image
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def render(self, screen):
        scaled_image = pygame.transform.scale(self.image, (self.width, self.height))
        screen.blit(scaled_image, (self.x, self.y))

