import pygame

class Renderer:
    def __init__(self, screen_size, title, entity_manager):
        self.screen = pygame.Surface(screen_size)
        self.window = pygame.display.set_mode(screen_size, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
        pygame.display.set_caption(title)
        self.assets = {}
        self.entity_manager = entity_manager
        self.original_size = screen_size

    def get_screen(self):
        return self.screen

    def load_asset(self, file_path, asset_name):
        self.assets[asset_name] = pygame.image.load(file_path).convert_alpha()

    def get_screen_size(self):
        return self.screen.get_size()

    def render(self):
        self.screen.fill((0, 0, 0))
        for entity in self.entity_manager.get_entities():
            entity.render(self.screen)
        x_ratio = self.original_size[0] / self.original_size[1]
        scaled_win = pygame.transform.scale(self.screen, (self.window.get_size()[1] * x_ratio, self.window.get_size()[1]))
        self.window.blit(scaled_win, ((self.window.get_size()[0] / 2) - (scaled_win.get_size()[0] / 2), 0))
        pygame.display.flip()
