from decimal import Decimal

class Animation:
    def __init__(self, name, image, frames, frame_rate, frame_width, frame_height, frames_x, frames_y, loop):
        self.image = image
        self.name = name
        self.frames = frames
        self.frame_rate = frame_rate
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.frames_x = frames_x
        self.frames_y = frames_y
        self.elapsed_time_since = 0
        self.current_frame = 0
        self.loop = loop
        self.animation_length = frame_rate * len(frames) * 1000
        self.elapsed_time = 0

    def get_current_frame(self):
        current_frame_x = (self.frames[self.current_frame] % self.frames_x) * self.frame_width
        current_frame_y = (self.frames[self.current_frame] // self.frames_x) * self.frame_height
        return self.image.subsurface((current_frame_x, current_frame_y, self.frame_width, self.frame_height))

    def update(self, dt):
        self.elapsed_time_since += dt
        self.elapsed_time += dt * 1000

        if self.elapsed_time_since > self.frame_rate:
            self.current_frame += 1
            self.elapsed_time_since = 0

            if self.current_frame >= len(self.frames) - 1:
                if self.loop:
                    self.current_frame = 0
                    self.elapsed_time = 0
                else:
                    self.current_frame = len(self.frames) - 1


class AnimationManager:
    def __init__(self, animation_data, assets):
        self.animations = {}
        self.current_animation = None
        self.load_animations(animation_data, assets)
   
    def load_animations(self, animation_data, assets):
        for animation_name, animation_info in animation_data.items():
            image_name = animation_info["image_name"]
            image = assets[image_name]
            frames = animation_info["frames"]
            frame_rate = animation_info["frame_rate"]
            frame_width = animation_info["info"]["frame_width"]
            frame_height = animation_info["info"]["frame_height"]
            frames_x = animation_info["info"]["frames_x"]
            frames_y = animation_info["info"]["frames_y"]
            loop = animation_info["info"]["loop"]
            animation = Animation(animation_name, image, frames, frame_rate, frame_width, frame_height, frames_x, frames_y, loop)
            self.animations[animation_name] = animation

    def set_animation(self, animation_name):
        if self.current_animation != animation_name:
            self.animations[animation_name].current_frame = 0
            self.animations[animation_name].elapsed_time = 0
            self.animations[animation_name].elapsed_time_since = 0

            # print('animation changed to: %s' % animation_name)
            self.current_animation = animation_name

    def get_animation(self):
        return self.animations[self.current_animation].get_current_frame()

    def is_animation_finished(self):
        if self.animations[self.current_animation].loop:
            return False
        else:
            # print(self.animations[self.current_animation].elapsed_time, self.animations[self.current_animation].animation_length)
            return (self.animations[self.current_animation].elapsed_time / self.animations[self.current_animation].animation_length) >= 1

    def get_animation_progress(self):
        # print(self.animations[self.current_animation].current_frame, '|', self.animations[self.current_animation].elapsed_time, self.animations[self.current_animation].animation_length)
        return self.animations[self.current_animation].elapsed_time / self.animations[self.current_animation].animation_length

    def update(self, dt):
        if len(self.animations):
            if self.current_animation and self.current_animation in self.animations:
                self.animations[self.current_animation].update(dt)

