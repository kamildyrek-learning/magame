import pygame
import json

import game.config as config
from game.render import Renderer 
from game.animation_manager import AnimationManager
from game.entity_manager import EntityManager

from entities.player import Player
from entities.platform import Platform
from entities.background import Background

pygame.init()

entity_manager = EntityManager()

assets = {}
assets["player"] = pygame.image.load("assets/images/player.png")
assets["default_bg"] = pygame.image.load("assets/images/bg02.png")

with open("game/animations.json") as json_file:
    animation_data = json.load(json_file)

animation_manager = AnimationManager(animation_data, assets)

renderer = Renderer((640, 480), "My Game", entity_manager)

screen_width, screen_height = renderer.get_screen_size()
player = Player(60, screen_height - 40, 32, 32, 14, 30, "player", animation_manager, entity_manager)
background = Background(0, 0, screen_width, screen_height, assets["default_bg"], "default_bg")

entity_manager.add_entity(background)
entity_manager.add_entity(player)
entity_manager.add_entity(Platform(526, 310, 150, 10, 'p0'))
entity_manager.add_entity(Platform(0, 394, 318, 400, 'p1'))
entity_manager.add_entity(Platform(420, 396, 300, 15, 'p2'))
entity_manager.add_entity(Platform(460, 392, 300, 400, 'p2'))
entity_manager.add_entity(Platform(-40, 0, 40, screen_height, 'p3'))
entity_manager.add_entity(Platform(screen_width + 40, 0, 40, screen_height, 'p4'))

running = True

clock = pygame.time.Clock()

while running:
    dt = clock.tick(80) / 1000  # limit the game to 60 FPS
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            running = False

    player.handle_input(events)
    animation_manager.update(dt)
    player.update()
    renderer.render()

pygame.quit()