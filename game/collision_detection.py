import math

def is_in_proximity(point1, point2, threshold):
    distance = math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)
    if distance < threshold:
        return True
    else:
        return False

class GrabData:
    def __init__(self, attach_point, value):
        self.value = value
        self.attach_point = attach_point

class Collision:
    def __init__(self):
        self.left = None
        self.right = None
        self.top = None
        self.bottom = None
        self.left_overlap = 0
        self.right_overlap = 0
        self.top_overlap = 0
        self.bottom_overlap = 0
        self.grab = GrabData((0,0), 0)
        self.ground_close_by = True
        self.ground_distance = 0

class CollisionDetector:
    def __init__(self, target, entity_manager):
        self.entity_manager = entity_manager
        self.target = target

    def get_collisions(self, entity_group = 'entities'):
        target_rect = self.target.rect
        target_sensor = self.target.sensor
        target_top = target_rect.midtop
        target_bottom = target_rect.midbottom
        target_upper_left = (target_rect.left, target_rect.top + (target_rect.height * 0.25))
        target_upper_right = (target_rect.right, target_rect.top + (target_rect.height * 0.25))
        target_lower_left = (target_rect.left, target_rect.top + (target_rect.height * 0.5))
        target_lower_right = (target_rect.right, target_rect.top + (target_rect.height * 0.5))

        collision = Collision()
        entities = self.entity_manager.get_entities(entity_group)
        colliding_entities = target_sensor.collidelistall(entities)
        collision.ground_close_by = False      

        for entity_index in colliding_entities:
            entity = entities[entity_index]
            if not entity == self.target:
                entity_rect = entity.rect
                bottom_overlap = target_rect.bottom - entity_rect.top
                top_overlap = entity_rect.bottom - target_rect.top
                right_overlap = target_rect.right - entity_rect.left
                left_overlap = entity_rect.right - target_rect.left

                # Standard collisions
                if entity_rect.collidepoint(target_bottom):
                    collision.bottom_overlap = bottom_overlap
                    collision.bottom = entity.name
                if entity_rect.collidepoint(target_top):
                    collision.top_overlap = top_overlap
                    collision.top = entity.name
                if (entity_rect.collidepoint(target_upper_right) or
                entity_rect.collidepoint(target_lower_right)):
                    collision.right_overlap = right_overlap
                    collision.right = entity.name
                if (entity_rect.collidepoint(target_upper_left) or
                entity_rect.collidepoint(target_lower_left)):
                    collision.left_overlap = left_overlap
                    collision.left = entity.name
                # Grabbing
                if is_in_proximity(target_upper_right, entity_rect.topleft, target_rect.width):
                    collision.grab = GrabData(entity_rect.topleft, 1)
                if is_in_proximity(target_upper_left, entity_rect.topright, target_rect.width):
                    collision.grab = GrabData(entity_rect.topright, -1)
                # Ground distance
                if entity_rect.midtop[1] < target_sensor.midbottom[1] and entity_rect.midtop[1] >= target_rect.midbottom[1] - 2:
                    collision.ground_close_by = True
                    collision.ground_distance = entity_rect.midtop[1] - target_rect.midbottom[1]

        return collision