class EntityManager:
    def __init__(self):
        self.entities = []
        self.player = []
        self.platforms = []
        self.enemies = []
        self.backgrounds = []

    def add_entity(self, entity):
        self.entities.append(entity)
        entity_group = getattr(self, entity.type, None)
        if entity_group is not None:
            entity_group.append(entity)

    def remove_entity(self, entity):
        self.entities.remove(entity)
        entity_group = getattr(self, entity.type, None)
        if entity_group is not None:
            entity_group.remove(entity)

    def get_entities(self, entity_group = 'entities'):
        entity_group = getattr(self, entity_group, None)
        if entity_group is not None:
            return entity_group
        else:
            return []

class EntityTypes:
    # entity key = entity group name
    PLAYER = 'player'
    PLATFORM = 'platforms'
    ENEMY = 'enemies'
    BACKGROUND = 'backgrounds'
